package pl.infoshareacademy.TranslateAPI;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TranslateAPITest {

	GoogleTranslate googleTranslate = new GoogleTranslate("KEY");
	
	@Test
	public void testTranslate(){
		
		String str = googleTranslate.translate("Hi This is test case", "EN", "FR");
		System.out.println(str);
	}
	
}
