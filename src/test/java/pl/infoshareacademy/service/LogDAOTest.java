package pl.infoshareacademy.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import pl.infoshareacademy.model.Log;

@RunWith(MockitoJUnitRunner.class)
public class LogDAOTest {
	@Mock
	private EntityManager em;

	@InjectMocks
	private LogDAO logDAO;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

	}

	
	@Test
	public void testGetAllLogs() {

		
		List<String> list = Mockito.mock(List.class);
		Query query = Mockito.mock(Query.class);
		Mockito.when(em.createQuery("FROM Log")).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(list);
		Mockito.when(list.isEmpty()).thenReturn(false);
		Mockito.when(list.size()).thenReturn(1);
		List<Log> logs = logDAO.getAllLogs();
		assertEquals(logs.size(), 1);
	}

	
	@Test
	public void testGetLogsByLevel(){
		
		
	}
}
