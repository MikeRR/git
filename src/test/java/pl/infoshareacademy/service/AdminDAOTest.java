/**
 * 
 */
package pl.infoshareacademy.service;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * @author AS47321
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AdminDAOTest {
	
	@InjectMocks
	private AdminsDAO adminDao;
	
	@Before
    public void init() {
		MockitoAnnotations.initMocks(this);
    }
	@Mock
	private  EntityManager em ; 
	
	
	@Test
    public void shouldFindAdminWithToken() throws Exception {
        // given
		String token = "abc";
		
		List<String> list = Mockito.mock(List.class);
		Query query = Mockito.mock(Query.class);
        Mockito.when(em.createQuery("SELECT e FROM Admins e WHERE e.token LIKE :token")).thenReturn(query);
		Mockito.when(query.setParameter("token", "abc")).thenReturn(query);
		Mockito.when(query.setMaxResults(5)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(list);
		Mockito.when(list.isEmpty()).thenReturn(false);
		
		
		boolean result = adminDao.findAdminByToken(token);
		assertTrue(result);
		
    }
	
	
	@Test
    public void shouldNotFindAdminWithToken() throws Exception {
        // given
		String token = "abc";
		
		List<String> list = Mockito.mock(List.class);
		Query query = Mockito.mock(Query.class);
        Mockito.when(em.createQuery("SELECT e FROM Admins e WHERE e.token LIKE :token")).thenReturn(query);
		Mockito.when(query.setParameter("token", "abc")).thenReturn(query);
		Mockito.when(query.setMaxResults(5)).thenReturn(query);
		Mockito.when(query.getResultList()).thenReturn(list);
		Mockito.when(list.isEmpty()).thenReturn(true);
		
		
		boolean result = adminDao.findAdminByToken(token);
		assertFalse(result );
		
    }
}
