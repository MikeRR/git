package pl.infoshareacademy.service;

import pl.infoshareacademy.model.Admins;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.List;

@Stateless
public class AdminsDAO {
    @PersistenceContext(unitName = "gity-db")
    public EntityManager em;

    public boolean findAdminByToken(String token) {
    	Query query =em.createQuery("SELECT e FROM Admins e WHERE e.token LIKE :token");
        List<Admins> result = query
                .setParameter("token", token)
                .setMaxResults(5)
                .getResultList();
        return !result.isEmpty();
    }
}
